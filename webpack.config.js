let Encore = require('@symfony/webpack-encore')

Encore
    .setOutputPath('src/Resources/public/build')
    .setPublicPath('/bundles/farvestadmin/build')

    .addStyleEntry('fab_admin', './assets/scss/fab_admin.scss')

    .configureBabel((babelConfig) => {
        const preset = babelConfig.presets.find(([name]) => name => "@babel/preset-env")
        if (preset !== undefined) {
            preset[1].useBuiltIns = "usage"
            preset[1].corejs = "3.0.1"
        }
    })
    .setManifestKeyPrefix('build')
    .disableSingleRuntimeChunk()
    .cleanupOutputBeforeBuild()
    .enableBuildNotifications()
    .enableSourceMaps(!Encore.isProduction())
    .enableVersioning(Encore.isProduction())

    .enableSassLoader()
    .enablePostCssLoader((options) => {
        options.config = {
            path: 'src/Resources/config/postcss.config.js'
        }
    })

if (!Encore.isProduction()) {
    Encore
        .copyFiles({
            from: './assets/images',
            to: 'images/[path][name].[ext]'
        })
} else {
    Encore
        .copyFiles({
            from: '.assets/images',
            to: 'images/[path][name].[hash:8].[ext]'
        })
}

module.exports = Encore.getWebpackConfig()
