<?php declare(strict_types=1);
/** *****************************************************************************************************************
 *  AdminConfig
 *  *****************************************************************************************************************
 *  @copyright 2020 Farvest
 *  @author Emmanuel Grosdemange <emmanuel.grosdemange@farvest.com>
 *  *****************************************************************************************************************
 *  Created: 2019/02/17
 *  ***************************************************************************************************************** */

namespace Farvest\AdminBundle\Service;

use Farvest\AdminBundle\Entity\AdminConfig;
use Farvest\AdminBundle\Entity\AdminEntity;
use Farvest\AdminBundle\Entity\AdminFormTemplate;
use Farvest\AdminBundle\Entity\AdminListTemplate;
use Farvest\AdminBundle\Entity\AdminMenu;
use Farvest\AdminBundle\Entity\Exception\EntityClassNotExistsException;
use Farvest\AdminBundle\Entity\Exception\FormTypeClassNotExistsException;
use Farvest\AdminBundle\Entity\Field\AdminField;
use Farvest\AdminBundle\Entity\Field\AdminFieldForm;
use Farvest\AdminBundle\Entity\Field\AdminFieldFormEntity;
use Farvest\AdminBundle\Entity\Field\AdminFieldFormInterface;
use Farvest\AdminBundle\Entity\Field\AdminFieldList;
use Farvest\AdminBundle\Utils\KeyAlreadyExistsException;
use Farvest\AdminBundle\Utils\KeyNotExistsException;
use Farvest\AdminBundle\Utils\YamlConfigParser;

/** *****************************************************************************************************************
 * Class AdminBuilder
 * ------------------------------------------------------------------------------------------------------------------
 * Build the the AdminConfig object with data from admin.yaml
 * ------------------------------------------------------------------------------------------------------------------
 * @package Farvest\AdminBundle\Service
 * ****************************************************************************************************************** */
class AdminBuilder
{
    /**
     * @var     array
     * -------------------------------------------------------------------------------------------------------------- */
    private $configArray;
    /**
     * @var     AdminConfig
     * -------------------------------------------------------------------------------------------------------------- */
    private $adminConfig;
    /**
     * @var     array
     * -------------------------------------------------------------------------------------------------------------- */
    private $errors;

    /** *************************************************************************************************************
     * AdminBuilder constructor.
     * --------------------------------------------------------------------------------------------------------------
     * @param   YamlConfigParser            $parser
     * @throws  KeyAlreadyExistsException
     * @throws  KeyNotExistsException
     * ************************************************************************************************************** */
    public function __construct(YamlConfigParser $parser)
    {
        $this->configArray = $parser->getConfig();
        $this->adminConfig = new AdminConfig();
        $this->errors = [];
        $this->hydrateAll();
    }

    /** *************************************************************************************************************
     * @throws  KeyAlreadyExistsException
     * @throws  KeyNotExistsException
     * ************************************************************************************************************** */
    private function hydrateAll(): void
    {
        $this->hydrateMenu();
        $this->hydrateEntity();
    }

    /** *************************************************************************************************************
     * @throws  KeyAlreadyExistsException
     * ************************************************************************************************************** */
    private function hydrateMenu(): void
    {
        $adminMenus = $this->configArray['admin']['menu'];
        if (null !== $adminMenus) {
            foreach ($adminMenus as $key => $config) {
                $menu = new AdminMenu();
                $menu
                    ->setMenuName($key)
                    ->setMenuLabel($config['label'])
                    ->setMenuRoles($config['roles'] ?? null);
                $this->adminConfig->addMenu($menu);
            }
        }
    }

    /** *************************************************************************************************************
     * @throws KeyAlreadyExistsException
     * @throws KeyNotExistsException
     * ************************************************************************************************************** */
    private function hydrateEntitY(): void
    {
        $adminEntities = $this->configArray['admin']['entity'];
        if (null !== $adminEntities) {
            foreach ($adminEntities as $key => $config) {
                $entity = new AdminEntity();
                $entity
                    ->setEntityName($key)
                    ->setEntityPluralName($config['plural_name'])
                    ->setEntityRoles($config['roles'] ?? null);

                try {
                    $entity->setEntityClass($config['class']);
                } catch (EntityClassNotExistsException $e) {
                    $this->errors[] = sprintf('Entity [%s][%s] : %s', $key, 'class', $e->getMessage());
                }

                $entity->setListTemplate($this->hydrateAdminListTemplate($key, $config));
                $entity->setFormTemplate($this->hydrateAdminFormTemplate($key, $config));

                $entity = $this->hydrateFields($entity, $config);

                $this->adminConfig->addEntity($entity);
            }
        }
    }

    /** *************************************************************************************************************
     * @param   string      $key
     * @param   array       $config
     * @return  AdminListTemplate
     * ************************************************************************************************************** */
    private function hydrateAdminListTemplate(string $key, array $config): AdminListTemplate
    {
        $entityListTemplate = new AdminListTemplate();

        if (!isset($config['list'])) {
            $entityListTemplate->setAuto(false);
            $this->errors[] = sprintf('Entity [%s][%s] : %s', $key, 'list', 'There is no "list" entry.');
            return $entityListTemplate;
        }

        $list = $config['list'];
        $entityListTemplate->setAuto($list['auto'] ?? false);
        if ($entityListTemplate->isAuto()) {
            $entityListTemplate->setTitle($list['title']);
            $entityListTemplate->setPagination($list['pagination'] ?? false);
            $entityListTemplate->setItemsPerPage($list['itemsPerPage'] ?? null);
            $entityListTemplate->setSearch($list['search'] ?? false);

            if (!isset($list['fields']) || null === $list['fields']) {
                $this->errors[] = sprintf('Entity [%s][%s][%s] : %s', $key, 'list', 'fields', 'Fields list for List Template cannot be empty/null.');
            } else {
                $entityListTemplate->setFields($list['fields']);
            }
            $entityListTemplate->setSearchFields($list['searchFields'] ?? []);
        }
        return $entityListTemplate;
    }

    /** *************************************************************************************************************
     * @param   string      $key
     * @param   array       $config
     * @return  AdminFormTemplate
     * ************************************************************************************************************** */
    private function hydrateAdminFormTemplate(string $key, array $config): AdminFormTemplate
    {
        $entityFormTemplate = new AdminFormTemplate();

        if (!isset($config['form'])) {
            $entityFormTemplate->setAuto(false);
            $this->errors[] = sprintf('Entity [%s][%s] : %s', $key, 'form', 'There is no "form" entry.');
            return $entityFormTemplate;
        }

        $form = $config['form'];
        $entityFormTemplate->setAuto($form['auto'] ?? false);
        if ($entityFormTemplate->isAuto()) {
            if (!isset($form['fields'])) {
                $this->errors[] = sprintf('Entity [%s][%s][%s] : %s', $key, 'form', 'fields', 'Form auto is set to true, then Fields list for Form Template cannot be empty/null.');
            } else {
                $entityFormTemplate->setFields($form['fields']);
            }
            return $entityFormTemplate;
        }

        if (!isset($form['form_type_class'])) {
            $this->errors[] = sprintf('Entity [%s][%s][%s] : %s', $key, 'form', 'form_type_class', 'Form auto is set to false, Then the form Type Class must be set.');
        } else {

            try {
                $entityFormTemplate->setFormTypeClass($form['form_type_class']);
            } catch (FormTypeClassNotExistsException $e) {
                $this->errors[] = sprintf('Entity [%s][%s][%s] : %s', $key, 'form', 'form_type_class', $e->getMessage());
            }
        }

        return $entityFormTemplate;
    }

    /** *************************************************************************************************************
     * @param   AdminEntity     $entity
     * @param   array           $config
     * @return  AdminEntity
     * @throws  KeyAlreadyExistsException
     * @throws  KeyNotExistsException
     * ************************************************************************************************************** */
    private function hydrateFields(AdminEntity $entity, array $config): AdminEntity
    {
        $fields = $config['fields'];

        foreach($fields as $key => $field) {
            $fieldObject = new AdminField();
            if (class_exists($entity->getEntityClass())) {
                if (!property_exists($entity->getEntityClass(), $key)) {
                    $this->errors[] = sprintf('Entity [%s] Field [%s] : %s', $entity->getEntityName(), $key, 'This attributes doesn\'t exists on the entity class.');
                }
            }
            $fieldObject->setFieldName($key);
            $fieldObject->setFieldLabel($field['label']);

            $adminFieldList = new AdminFieldList();
            $adminFieldList->setSorted($field['list']['sorted'] ?? false);
            $adminFieldList->setListDisplay($field['list']['list_display'] ?? null);
            $adminFieldList->setCssClasses($field['list']['css_classes'] ?? null);
            $adminFieldList->setStyle($field['list']['style'] ?? null);

            $fieldObject->setFieldList($adminFieldList);
            $fieldObject->setFieldForm($this->hydrateFieldsForm($entity->getEntityName(), $key, $field));

            $entity->addEntityField($fieldObject);
        }
        return $entity;
    }

    /** *************************************************************************************************************
     * @param   string      $entityName
     * @param   string      $fieldName
     * @param   array       $field
     * @return  AdminFieldFormInterface|null
     * @throws  KeyAlreadyExistsException
     * @throws  KeyNotExistsException
     * ************************************************************************************************************** */
    private function hydrateFieldsForm(string $entityName, string $fieldName, array $field): ?AdminFieldFormInterface
    {
        if (isset($field['form'])) {
            $form = $field['form'];
            $formFieldType = $form['type_class'];

            if ('Symfony\Component\Form\Extension\Core\Type\TextType' === $formFieldType) {
                return $this->hydrateFieldForm($form);
            } elseif ('Symfony\Bridge\Doctrine\Form\Type\EntityType' === $formFieldType) {
                return $this->hydrateFieldFormEntity($entityName, $fieldName, $form);
            }
        }
        return null;
    }

    /** *************************************************************************************************************
     * @param   array       $form
     * @return  AdminFieldFormInterface
     * ************************************************************************************************************** */
    private function hydrateFieldForm(array $form): AdminFieldFormInterface
    {
        $adminFormField = new AdminFieldForm();

        return $this->hydrateCommonField($form, $adminFormField);
    }

    /** *************************************************************************************************************
     * Hydrate information common with all fields
     *
     * @param   array                   $form
     * @param   AdminFieldFormInterface $adminFormField
     * @return  AdminFieldFormInterface
     * ************************************************************************************************************** */
    private function hydrateCommonField(array $form, AdminFieldFormInterface $adminFormField)
    {
        $adminFormField->setTypeClass($form['type_class']);
        foreach ($form['options'] as $key => $option) {
            $adminFormField->addOption($option, $key);
        }

        return $adminFormField;
    }

    /** *************************************************************************************************************
     * @param   string      $entityName
     * @param   string      $fieldName
     * @param   array       $form
     * @return  AdminFieldFormInterface
     * @throws  KeyAlreadyExistsException
     * @throws  KeyNotExistsException
     * ************************************************************************************************************** */
    private function hydrateFieldFormEntity(string $entityName, string $fieldName, array $form): AdminFieldFormInterface
    {
        $adminFieldFormEntity = new AdminFieldFormEntity();

        $adminFieldFormEntity->setEntity($form['entity']);
        foreach ($form['repository'] as $key => $value) {
            if (in_array($key, ['class', 'method'])) {
                if ('class' === $key && !class_exists($value)) {
                    $this->errors[] = sprintf(
                        'Entity [%s] Field [%s][%s] : %s',
                        $entityName,
                        $fieldName,
                        'repository',
                        sprintf('The class "%s" doesn\'t exists.', $value)
                    );
                    $value = null;
                }
                /**
                 * /!\ 'method' must be defined after 'class' /!\
                 */
                if ('method' === $key && null !== $adminFieldFormEntity->getRepository()->get('class'))
                {
                    $repoClass = $adminFieldFormEntity->getRepository()->get('class');
                    if (!method_exists($repoClass, $value)) {
                        $this->errors[] = sprintf(
                            'Entity [%s] Field [%s][%s] : %s',
                            $entityName,
                            $fieldName,
                            'repository',
                            sprintf('The method "%s" doesn\'t exists in class "%s".', $value, $repoClass)
                        );
                        $value = null;
                    }
                }
                $adminFieldFormEntity->addInRepository($key, $value);
            }
        }

        $adminFieldFormEntity = $this->hydrateCommonField($form, $adminFieldFormEntity);
        return $adminFieldFormEntity;
    }

    /** *************************************************************************************************************
     * @return  AdminConfig
     * ************************************************************************************************************** */
    public function getAdminConfig(): AdminConfig
    {
        return $this->adminConfig;
    }

    /** *************************************************************************************************************
     * @return  array
     * ************************************************************************************************************** */
    public function getBuilderErrors(): array
    {
        return $this->errors;
    }
}