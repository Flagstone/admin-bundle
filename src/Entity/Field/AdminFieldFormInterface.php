<?php


namespace Farvest\AdminBundle\Entity\Field;


use Farvest\AdminBundle\Utils\KeyValueCollection;

interface AdminFieldFormInterface
{
    public function getTypeClass(): string;
    public function getOptions(): KeyValueCollection;

    public function addOption($option, $key): self;
    public function removeOption($option): self;
}