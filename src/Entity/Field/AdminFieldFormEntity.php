<?php declare(strict_types=1);
/** *****************************************************************************************************************
 *  AdminFieldFormEntity
 *  *****************************************************************************************************************
 *  @copyright 2020 Farvest
 *  @author Emmanuel Grosdemange <emmanuel.grosdemange@farvest.com>
 *  *****************************************************************************************************************
 *  Created: 2019/02/19
 *  ***************************************************************************************************************** */
namespace Farvest\AdminBundle\Entity\Field;

use Farvest\AdminBundle\Utils\KeyAlreadyExistsException;
use Farvest\AdminBundle\Utils\KeyValueCollection;

/** *****************************************************************************************************************
 * Class AdminBuilder
 * ------------------------------------------------------------------------------------------------------------------
 * Contains the list of entity fields characteristics
 * ------------------------------------------------------------------------------------------------------------------
 * @package Farvest\AdminBundle\Entity\Field
 * ****************************************************************************************************************** */
class AdminFieldFormEntity extends AbstractAdminFieldForm
{
    /**
     * @var     string
     * -------------------------------------------------------------------------------------------------------------- */
    private $entity;
    /**
     * @var     KeyValueCollection
     * -------------------------------------------------------------------------------------------------------------- */
    private $repository;

    public function __construct()
    {
        parent::__construct();
        $this->repository = new KeyValueCollection();
    }

    public function getEntity(): string
    {
        return $this->entity;
    }

    public function getRepository(): KeyValueCollection
    {
        return $this->repository;
    }

    public function setEntity(?string $entity): AdminFieldFormInterface
    {
        $this->entity = $entity;
        return $this;
    }

    /**
     * @param   string      $key
     * @param   string      $value
     * @return  AdminFieldFormInterface
     * @throws  KeyAlreadyExistsException
     */
    public function addInRepository(string $key, ?string $value): AdminFieldFormInterface
    {
        $this->repository->add($value, $key);
        return $this;
    }
}