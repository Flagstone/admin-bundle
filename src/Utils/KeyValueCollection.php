<?php declare(strict_types=1);
/** *****************************************************************************************************************
 *  KeyValueCollection
 *  *****************************************************************************************************************
 *  @copyright 2020 Farvest
 *  @author Emmanuel Grosdemange <emmanuel.grosdemange@farvest.com>
 *  *****************************************************************************************************************
 *  Created: 2019/02/14
 *  ***************************************************************************************************************** */

namespace Farvest\AdminBundle\Utils;

/** *****************************************************************************************************************
 * Class KeyValueCollection
 * ------------------------------------------------------------------------------------------------------------------
 * Define a key/value collection class.
 * ------------------------------------------------------------------------------------------------------------------
 * @package Farvest\AdminBundle\Utils
 * ****************************************************************************************************************** */
class KeyValueCollection
{
    /**
     * @var     array       The collection array
     * -------------------------------------------------------------------------------------------------------------- */
    private $items;

    /** *************************************************************************************************************
     * KeyValueCollection constructor.
     * ************************************************************************************************************** */
    public function __construct()
    {
        $this->items = [];
    }

    /** *************************************************************************************************************
     * Get all elements of the collection
     * --------------------------------------------------------------------------------------------------------------
     * @return  iterable
     * ************************************************************************************************************** */
    public function getAll(): iterable
    {
        return $this->items;
    }

    /** *************************************************************************************************************
     * Add an element to the collection, defined by its key (mandatory)
     * --------------------------------------------------------------------------------------------------------------
     * @param   string              $key
     * @param   mixed               $item
     * @return  KeyValueCollection
     * @throws  KeyAlreadyExistsException
     * ************************************************************************************************************** */
    public function add($item, ?string $key = null): self
    {
        if (null !== $key) {
            if (!array_key_exists($key, $this->items)) {
                $this->items[$key] = $item;
                return $this;
            }
            throw new KeyAlreadyExistsException(sprintf('The key %s already exists on the collection', $key));
        }
        $this->items[] = $item;
        return $this;
    }

    /** *************************************************************************************************************
     * Remove an element from the collection, defined by its key
     * --------------------------------------------------------------------------------------------------------------
     * @param   string              $key
     * @return  KeyValueCollection
     * @throws  KeyNotExistsException
     * ************************************************************************************************************** */
    public function remove(?string $key): self
    {
        if (null !== $key) {
            if (array_key_exists($key, $this->items)) {
                unset($this->items[$key]);
            }
            return $this;
        }
        throw new KeyNotExistsException(sprintf('The key %s doesn\'t exists on this collection.', $key));
    }

    /** *************************************************************************************************************
     * Remove an element from the collection, defined by its value (object content)
     * --------------------------------------------------------------------------------------------------------------
     * @param   object              $item
     * @return  KeyValueCollection
     * ************************************************************************************************************** */
    public function removeElement($item): self
    {
        foreach ($this->items as $key => $searchItem) {
            if ($item === $searchItem) {
                unset($this->items[$key]);
                return $this;
            }
        }
        return $this;
    }

    /** *************************************************************************************************************
     * Return the value of the collection element by its key
     * --------------------------------------------------------------------------------------------------------------
     * @param   string              $key
     * @return  mixed|null
     * @throws  KeyNotExistsException
     * ************************************************************************************************************** */
    public function get(?string $key)
    {
        if (null !== $key) {
            try {
                return $this->items[$key];
            } catch (\ErrorException $e) {
                throw new KeyNotExistsException(sprintf('The key %s doesn\'t exists on this collection.', $key));
            }
        }
        throw new KeyNotExistsException('The key cannot be null.');
    }
}