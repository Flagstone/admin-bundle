<?php declare(strict_types=1);
/** *****************************************************************************************************************
 *  AdminMenu
 *  *****************************************************************************************************************
 *  @copyright 2020 Farvest
 *  @author Emmanuel Grosdemange <emmanuel.grosdemange@farvest.com>
 *  *****************************************************************************************************************
 *  Created: 2019/02/17
 *  ***************************************************************************************************************** */

namespace Farvest\AdminBundle\Entity;

use Farvest\AdminBundle\Entity\Exception\EntityClassNotExistsException;
use Farvest\AdminBundle\Entity\Exception\FormTypeClassNotExistsException;
use Farvest\AdminBundle\Entity\Field\AdminField;
use Farvest\AdminBundle\Entity\Field\AdminFieldInterface;
use Farvest\AdminBundle\Utils\KeyAlreadyExistsException;
use Farvest\AdminBundle\Utils\KeyValueCollection;

/** *****************************************************************************************************************
 * Class AdminEntity
 * ------------------------------------------------------------------------------------------------------------------
 * @package Farvest\AdminBundle\Entity
 * ****************************************************************************************************************** */
class AdminEntity
{
    /**
     * @var     string              The name of the entity class
     * -------------------------------------------------------------------------------------------------------------- */
    private $entityName;
    /**
     * @var     string              The full class name of the entity
     * -------------------------------------------------------------------------------------------------------------- */
    private $entityClass;
    /**
     * @var     string              The entity name in plural mode
     * -------------------------------------------------------------------------------------------------------------- */
    private $entityPluralName;
    /**
     * @var     array               List of roles empowered to modify the entity
     * -------------------------------------------------------------------------------------------------------------- */
    private $entityRoles;
    /**
     * @var     KeyValueCollection  Collection of fields objects presents in the entity that must be displayed
     * -------------------------------------------------------------------------------------------------------------- */
    private $entityFields;
    /**
     * @var     AdminListTemplate   Object containing informations about dynamic entity list creation
     * -------------------------------------------------------------------------------------------------------------- */
    private $listTemplate;
    /**
     * @var     AdminFormTemplate   Object containing informations about dynamic entity form creation
     * -------------------------------------------------------------------------------------------------------------- */
    private $formTemplate;
    /**
     * @var     AdminConfig         Link to the base AdminConfig class
     * -------------------------------------------------------------------------------------------------------------- */
    private $config;

    /** *************************************************************************************************************
     * AdminEntity constructor.
     * Initialize arrays
     * ************************************************************************************************************** */
    public function __construct()
    {
        $this->entityFields = new KeyValueCollection();
    }

    /** -----*-----*-----*-----*-----*-----*-----*-----*-----*-----*-----*-----*-----*-----*-----*-----*-----*-----*-
     * Getters
     * -----*-----*-----*-----*-----*-----*-----*-----*-----*-----*-----*-----*-----*-----*-----*-----*-----*-----*-- */

    /** *************************************************************************************************************
     * @return string
     * ************************************************************************************************************** */
    public function getEntityName(): string
    {
        return $this->entityName;
    }

    /** *************************************************************************************************************
     * @return string
     * ************************************************************************************************************** */
    public function getEntityClass(): string
    {
        return $this->entityClass;
    }

    /** *************************************************************************************************************
     * @return string
     * ************************************************************************************************************** */
    public function getEntityPluralName(): string
    {
        return $this->entityPluralName;
    }

    /** *************************************************************************************************************
     * @return array
     * ************************************************************************************************************** */
    public function getEntityRole(): ?array
    {
        return $this->entityRoles;
    }

    /** *************************************************************************************************************
     * @return KeyValueCollection
     * ************************************************************************************************************** */
    public function getFields(): KeyValueCollection
    {
        return $this->entityFields;
    }

    /** *************************************************************************************************************
     * @return AdminListTemplate
     * ************************************************************************************************************** */
    public function getListTemplate()
    {
        return $this->listTemplate;
    }

    /** *************************************************************************************************************
     * @return AdminFormTemplate
     * ************************************************************************************************************** */
    public function getFormTemplate()
    {
        return $this->formTemplate;
    }

    /** *************************************************************************************************************
     * @return  AdminConfig
     * ************************************************************************************************************** */
    public function getConfig(): AdminConfig
    {
        return $this->config;
    }

    /** -----*-----*-----*-----*-----*-----*-----*-----*-----*-----*-----*-----*-----*-----*-----*-----*-----*-----*-
     * Setters
     * -----*-----*-----*-----*-----*-----*-----*-----*-----*-----*-----*-----*-----*-----*-----*-----*-----*-----*-- */

    /** *************************************************************************************************************
     * @param   string|null             $entityName
     * @return  AdminEntity
     * ************************************************************************************************************** */
    public function setEntityName(?string $entityName): self
    {
        $this->entityName = $entityName;
        return $this;
    }

    /** *************************************************************************************************************
     * @param   string|null             $entityClass
     * @return  AdminEntity
     * @throws  EntityClassNotExistsException
     * ************************************************************************************************************** */
    public function setEntityClass(?string $entityClass): self
    {
        if (class_exists($entityClass)) {
            $this->entityClass = $entityClass;
            return $this;
        }
        throw new EntityClassNotExistsException(sprintf('Class "%s" doesn\'t exists.', $entityClass));
    }

    /** *************************************************************************************************************
     * @param   string|null             $entityPluralName
     * @return  AdminEntity
     * ************************************************************************************************************** */
    public function setEntityPluralName(?string $entityPluralName): self
    {
        $this->entityPluralName = $entityPluralName;
        return $this;
    }

    /** *************************************************************************************************************
     * @param   array                   $entityRoles
     * @return  AdminEntity
     * ************************************************************************************************************** */
    public function setEntityRoles(?array $entityRoles): self
    {
        $this->entityRoles = $entityRoles;
        return $this;
    }

    /** *************************************************************************************************************
     * @param   AdminListTemplate        $listTemplate
     * @return  AdminEntity
     * ************************************************************************************************************** */
    public function setListTemplate(AdminListTemplate $listTemplate): self
    {
        $this->listTemplate = $listTemplate;
        return $this;
    }

    /** *************************************************************************************************************
     * @param   AdminFormTemplate       $formTemplate
     * @return  AdminEntity
     * ************************************************************************************************************** */
    public function setFormTemplate(AdminFormTemplate $formTemplate): self
    {
        $this->formTemplate = $formTemplate;
        return $this;
    }

    /** *************************************************************************************************************
     * @param   AdminConfig             $config
     * @return  AdminEntity
     * ************************************************************************************************************** */
    public function setConfig(AdminConfig $config): self
    {
        $this->config = $config;
        return $this;
    }

    /** -----*-----*-----*-----*-----*-----*-----*-----*-----*-----*-----*-----*-----*-----*-----*-----*-----*-----*-
     * Others
     * -----*-----*-----*-----*-----*-----*-----*-----*-----*-----*-----*-----*-----*-----*-----*-----*-----*-----*-- */

    /** *************************************************************************************************************
     * @param   AdminField              $entityField
     * @return  AdminEntity
     * @throws  KeyAlreadyExistsException
     * ************************************************************************************************************** */
    public function addEntityField(AdminField $entityField): self
    {
        $this->entityFields->add($entityField, $entityField->getFieldName());
        $entityField->setEntity($this);
        return $this;
    }

    /** *************************************************************************************************************
     * @param   AdminField              $entityField
     * @return  AdminEntity
     * ************************************************************************************************************** */
    public function removeEntityField(AdminField $entityField): self
    {
        $this->entityFields->removeElement($entityField);
        $entityField->setEntity(null);
        return $this;
    }
}