<?php
/** *****************************************************************************************************************
 *  AddConfigListener.php
 *  *****************************************************************************************************************
 *  @copyright 2020 Farvest
 *  @author Emmanuel Grosdemange <emmanuel.grosdemange@farvest.com>
 *  *****************************************************************************************************************
 *  Created: 2019/02/17
 *  ***************************************************************************************************************** */

namespace Farvest\AdminBundle\EventListener;

use Farvest\AdminBundle\Service\AdminBuilder;
use Symfony\Component\HttpKernel\Event\RequestEvent;
use Twig\Environment;

/** *****************************************************************************************************************
 * Class AddConfigListener
 * ------------------------------------------------------------------------------------------------------------------
 * Add all the admin config object on each page that have /admin/ in their Uri.
 * Then, no need to add this on each Controller of the application.
 * ------------------------------------------------------------------------------------------------------------------
 * @package Farvest\AdminBundle\EventListener
 * ****************************************************************************************************************** */
class AddConfigListener
{
    private $twig;
    private $adminBuilder;

    public function __construct(Environment $twig, AdminBuilder $adminBuilder)
    {
        $this->twig = $twig;
        $this->adminBuilder = $adminBuilder;
    }

    /** *************************************************************************************************************
     * @param   RequestEvent    $event
     * ************************************************************************************************************** */
    public function onKernelRequest(RequestEvent $event)
    {
        if (strpos($event->getRequest()->getRequestUri(), '/admin/') !== false) {
            $config = $this->adminBuilder->getAdminConfig();
            $errors = $this->adminBuilder->getBuilderErrors();
            $this->twig->addGlobal('adminConfig', $config);
            $this->twig->addGlobal('builderErrors', $errors);
        } else {
            $this->twig->addGlobal('adminConfig', '');
            $this->twig->addGlobal('builderErrors', []);
        }
    }
}
