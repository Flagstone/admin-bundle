<?php declare(strict_types=1);
/** *****************************************************************************************************************
 *  YamlConfigParser
 *  *****************************************************************************************************************
 *  @copyright 2020 Farvest
 *  @author Emmanuel Grosdemange <emmanuel.grosdemange@farvest.com>
 *  *****************************************************************************************************************
 *  Created: 2019/02/17
 *  ***************************************************************************************************************** */

namespace Farvest\AdminBundle\Utils;

use Symfony\Component\DependencyInjection\ParameterBag\ParameterBagInterface;
use Symfony\Component\HttpKernel\KernelInterface;
use Symfony\Component\Yaml\Exception\ParseException;
use Symfony\Component\Yaml\Yaml;
use InvalidArgumentException;

/** *****************************************************************************************************************
 * Class YamlConfigParser
 * ------------------------------------------------------------------------------------------------------------------
 * Parse the admin.yaml file
 * ------------------------------------------------------------------------------------------------------------------
 * @package Farvest\AdminBundle\Utils
 * ****************************************************************************************************************** */
class YamlConfigParser
{
    /**
     * @var     KernelInterface
     * -------------------------------------------------------------------------------------------------------------- */
    private $kernel;
    /**
     * @var     ParameterBagInterface
     * -------------------------------------------------------------------------------------------------------------- */
    private $params;
    /**
     * @var     array
     * -------------------------------------------------------------------------------------------------------------- */
    private $yaml;

    /** *************************************************************************************************************
     * YamlConfigParser constructor.
     * --------------------------------------------------------------------------------------------------------------
     * @param   KernelInterface         $kernel
     * @param   ParameterBagInterface   $params
     * @throws  AdminYamlParserException
     * ************************************************************************************************************** */
    public function __construct(KernelInterface $kernel, ParameterBagInterface $params)
    {
        $this->kernel = $kernel;
        $this->params = $params;
        $this->getParsedFile();
    }

    /** *************************************************************************************************************
     * @throws  AdminYamlParserException
     * ************************************************************************************************************** */
    public function getParsedFile()
    {
        $configFile = '';
        try {
            $configFile = $this->params->get('admin_config');
            $this->yaml = Yaml::parseFile($this->kernel->getProjectDir().$configFile);
        } catch (ParseException $e) {
            throw new AdminYamlParserException(
                sprintf('Fatal Error: %s doesn\'t exists.',$this->kernel->getProjectDir().$configFile)
            );
        } catch (\InvalidArgumentException $e) {
            throw new AdminYamlParserException(
                sprintf('Fatal Error: \'admin_config\' entry must be defined in the services.yaml files, in parameters sectio, with the path of the admin config file.')
            );
        }
    }

    /** *************************************************************************************************************
     * @return  array|null
     * ************************************************************************************************************** */
    public function getConfig(): ?array
    {
        return $this->yaml;
    }
}
