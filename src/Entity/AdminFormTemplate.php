<?php declare(strict_types=1);
/** *****************************************************************************************************************
 *  AdminFormTemplate
 *  *****************************************************************************************************************
 *  @copyright 2020 Farvest
 *  @author Emmanuel Grosdemange <emmanuel.grosdemange@farvest.com>
 *  *****************************************************************************************************************
 *  Created: 2019/02/18
 *  ***************************************************************************************************************** */

namespace Farvest\AdminBundle\Entity;

use Farvest\AdminBundle\Entity\Exception\EntityClassNotExistsException;
use Farvest\AdminBundle\Entity\Exception\FormTypeClassNotExistsException;

/** *****************************************************************************************************************
 * Class AdminFormTemplate
 * ------------------------------------------------------------------------------------------------------------------
 * Informations to build automatic forms
 * ------------------------------------------------------------------------------------------------------------------
 * @package Farvest\AdminBundle\Entity
 * ****************************************************************************************************************** */
class AdminFormTemplate
{
    /**
     * @var     bool
     * -------------------------------------------------------------------------------------------------------------- */
    private $auto;
    /**
     * @var     string
     * -------------------------------------------------------------------------------------------------------------- */
    private $formTypeClass;
    /**
     * @var     array
     * -------------------------------------------------------------------------------------------------------------- */
    private $fields;

    /** -----*-----*-----*-----*-----*-----*-----*-----*-----*-----*-----*-----*-----*-----*-----*-----*-----*-----*-
     * Getters
     * -----*-----*-----*-----*-----*-----*-----*-----*-----*-----*-----*-----*-----*-----*-----*-----*-----*-----*-- */

    /**
     * @return bool
     */
    public function isAuto(): bool
    {
        return $this->auto;
    }

    /**
     * @return string
     */
    public function getFormTypeClass(): string
    {
        return $this->formTypeClass;
    }

    /**
     * @return array
     */
    public function getFields(): ?array
    {
        return $this->fields;
    }

    /** -----*-----*-----*-----*-----*-----*-----*-----*-----*-----*-----*-----*-----*-----*-----*-----*-----*-----*-
     * Setters
     * -----*-----*-----*-----*-----*-----*-----*-----*-----*-----*-----*-----*-----*-----*-----*-----*-----*-----*-- */

    /**
     * @param bool $auto
     * @return $this
     */
    public function setAuto(bool $auto): self
    {
        $this->auto =$auto;
        return $this;
    }

    /**
     * @param   string      $formTypeClass
     * @return  $this
     * @throws  FormTypeClassNotExistsException
     */
    public function setFormTypeClass(string $formTypeClass): self
    {
        if (class_exists($formTypeClass)) {
            $this->formTypeClass = $formTypeClass;
            return $this;
        }
        throw new FormTypeClassNotExistsException(sprintf('Class "%s" doesn\'t exists.', $formTypeClass));
    }

    /**
     * @param array $fields
     * @return $this
     */
    public function setFields(?array $fields): self
    {
        $this->fields = $fields;
        return $this;
    }
}
