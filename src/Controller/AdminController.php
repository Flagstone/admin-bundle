<?php


namespace Farvest\AdminBundle\Controller;


use Farvest\AdminBundle\Entity\AdminConfig;
use Farvest\AdminBundle\Entity\AdminEntity;
use Farvest\AdminBundle\Service\AdminBuilder;
use Farvest\AdminBundle\Utils\KeyNotExistsException;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;
use Symfony\Component\Routing\Annotation\Route;

/**
 * Class AdminController
 * @package Farvest\AdminBundle\Controller
 *
 * @Route(
 *     "/admin"
 * )
 */
class AdminController extends AbstractController
{
    /**
     * @var     AdminConfig
     */
    private $adminConfig;

    public function __construct(AdminBuilder $builder)
    {
        $this->adminConfig = $builder->getAdminConfig();
    }

    /**
     * @Route(
     *     path = "/",
     *     name="admin_home"
     * )
     */
    public function home()
    {
        return $this->render('@FarvestAdmin/base.html.twig');
    }

    /**
     * @param   string|null $entity
     * @return  Response
     * @Route(
     *     path = "/{entity}/list",
     *     name = "admin_list"
     * )
     */
    public function list(?string $entity)
    {
        try {
            /** @var AdminEntity $adminEntity */
            $adminEntity = $this->adminConfig->getEntities()->get($entity);
        } catch (KeyNotExistsException $e) {
            throw new NotFoundHttpException($e->getMessage().' "'.$entity.'" list cannot be displayed');
        }

        $data = $this->getDoctrine()->getManager()->getRepository($adminEntity->getEntityClass())->findAll();

        if (true === $adminEntity->getListTemplate()->isAuto()) {
            return $this->render(
                '@FarvestAdmin/pages/list.html.twig',
                [
                    'entityList' => $adminEntity->getListTemplate(),
                    'entityFields' => $adminEntity->getFields(),
                    'data' => $data
                ]
            );
        } else {
            return $this->render('admin/'.$entity.'_list.html.twig');
        }
    }
}